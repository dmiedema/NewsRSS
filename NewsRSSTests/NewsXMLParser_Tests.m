//
//  NewsXMLParser_Tests.m
//  NewsRSSTests
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NewsXMLParser.h"

@interface NewsXMLParserDelegate_Tests : NSObject <NewsXMLParserDelegate>
@property (nonatomic, nullable, strong) NSError *error;
@property (nonatomic, nonnull, copy) NSArray *results;
@property (nonatomic) BOOL finished;
@end

@interface NewsXMLParser_Tests : XCTestCase
@property (nonatomic, nullable, strong) NewsXMLParser *parser;
@property (nonatomic, nullable, strong) NewsXMLParserDelegate_Tests *delegate;
@property (nonatomic, nonnull, strong) NSData *rssFeedData;
@end


@implementation NewsXMLParser_Tests

- (void)setUp {
    _rssFeedData = [NSData dataWithContentsOfURL:[NSBundle.mainBundle URLForResource:@"news" withExtension:@"rss"]];

    _delegate = [NewsXMLParserDelegate_Tests new];
    _parser = [NewsXMLParser newsXMLParserWithDelegate:_delegate];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    _parser = nil;
    _delegate = nil;
}

- (void)testParsingValidRSSXML {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    XCTAssertNoThrow([_parser parse:_rssFeedData], @"parsing should not fail");
    XCTAssertTrue(_delegate.finished, @"Parser failed to finsh parsing");
    XCTAssertNil(_delegate.error, @"Parser got an error with an valid RSS Feed");
    XCTAssertTrue(_delegate.results.count > 1, @"Parser results was empty? Contained %@ items", @(_delegate.results.count));
}

- (void)testParsingInvalidRSSXML {
    NSString *rssFeed = [[NSString alloc] initWithData:_rssFeedData encoding:NSUTF8StringEncoding];
    _rssFeedData = [[rssFeed stringByReplacingOccurrencesOfString:@"<" withString:@"_lol_invalid_xml_now_"] dataUsingEncoding:NSUTF8StringEncoding];

#if DEBUG
    XCTAssertThrows([_parser parse:_rssFeedData], @"In DEBUG parse failures should throw an exception");
#else
    [_parser parse:_rssFeedData];
    XCTAssertTrue(_delegate.finished, @"Parser failed to finsh parsing");
    XCTAssertNotNil(_delegate.error, @"Parser did not error with an invalid RSS Feed");
    XCTAssertTrue(_delegate.results.count == 0, @"Parser results was not empty. Contained %@ items", @(_delegate.results.count));
#endif
}

- (void)testPerformanceValidRSSXMLParsing {
    // This is an example of a performance test case.
    [self measureBlock:^{
        [self.parser parse:self.rssFeedData];
    }];
}

@end


@implementation NewsXMLParserDelegate_Tests

- (void)newsXMLParserDidEndParsingWithResults:(NSArray<NewsItem *> *)results error:(NSError *)error {
    self.finished = YES;
    self.results = results;
    self.error = error;
}

@end
