//
//  NewsTableViewDatasource_Tests.m
//  NewsRSSTests
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NewsItem.h"
#import "NewsTableViewDatasource.h"

@interface NewsTableViewDatasource_Tests : XCTestCase
@property (nonatomic, nullable, strong) NSManagedObjectContext *context;
@property (nonatomic, nullable, strong) NewsItem *item;
@property (nonatomic, nullable, strong) NewsTableViewDatasource *datasource;
@end

@implementation NewsTableViewDatasource_Tests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSPersistentContainer *container = [[NSPersistentContainer alloc] initWithName:@"NewsRSS"];

    _context = container.viewContext;

    _item = [[NewsItem alloc] initWithContext:_context];
    _item.date = [NSDate date];
    _item.guid = @"Best GUID";
    _item.title = @"Best Title";

    for (NSInteger i = 1; i < 11; i++) {
        __auto_type item = [[NewsItem alloc] initWithContext:_context];
        item.date = [NSDate dateWithTimeIntervalSinceNow:-(i * 2)];
    }

    [_context save:nil];

    _datasource = [[NewsTableViewDatasource alloc] initWithContext:_context];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    _context = nil;
}

- (void)testFilteredNewsItems {
    __auto_type unFiltered = [_datasource newsItemsFilteredWithText:nil];
    XCTAssertTrue(unFiltered.count == 11,
                  @"Should have had 11 items but instead had %@",
                  @(unFiltered.count));

    __auto_type expectedResults = @{
                                    @"B": @1,
                                    @"b": @1,
                                    @"bb": @0,
                                    @"best": @1,
                                    @"BEST": @1,
                                    @"burp": @0,
                                    @"lolnope": @0
                                    };

    for (NSString *key in expectedResults.allKeys) {
        NSNumber *assumedCount = expectedResults[key];
        __auto_type count = [_datasource newsItemsFilteredWithText:key].count;
        __auto_type expectedCount = assumedCount.integerValue;
        XCTAssertTrue(count == expectedCount,
                      @"Count should have been %@ but instead was %@",
                      @(count), @(expectedCount));
    }

}
@end
