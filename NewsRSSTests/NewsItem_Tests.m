//
//  NewsItem_Tests.m
//  NewsRSSTests
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NewsItem.h"

@interface NewsItem_Tests : XCTestCase
@property (nonatomic, nullable, strong) NSManagedObjectContext *context;
@property (nonatomic, nullable, strong) NewsItem *item;
@end

@implementation NewsItem_Tests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSPersistentContainer *container = [[NSPersistentContainer alloc] initWithName:@"NewsRSS"];

    _context = container.viewContext;

    _item = [[NewsItem alloc] initWithContext:_context];
    _item.date = [NSDate date];
    _item.guid = @"Best GUID";

    for (NSInteger i = 1; i < 11; i++) {
        __auto_type item = [[NewsItem alloc] initWithContext:_context];
        item.date = [NSDate dateWithTimeIntervalSinceNow:-(i * 2)];
    }

    [_context save:nil];
}

- (void)tearDown {
    _context = nil;
}

- (void)testAllItems {
    NSArray *allItems = [NewsItem allItemsInContext:_context];

    XCTAssertTrue(allItems.count == 11,
                  @"Should have had 11 items, only had %@",
                  @(allItems.count));
}

- (void)testAllItemsSortedByDate {
    __auto_type allItems = [NewsItem allItemsSortedByDateInContext:_context];

    XCTAssertTrue(allItems.count == 11,
                  @"Should have had 11 items, only had %@",
                  @(allItems.count));
    XCTAssertTrue([allItems.firstObject.guid isEqualToString:_item.guid],
                  @"First item should have been %@ but instead was %@", _item, allItems.firstObject);
}

@end
