//
//  NSDate+Helpers_Tests.m
//  NewsRSSTests
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDate+Helpers.h"

@interface NSDate_Helpers_Tests : XCTestCase

@end

@implementation NSDate_Helpers_Tests

- (void)testDisplayString {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSString *displayString = NSDate.date.dmm_displayString;
    XCTAssertNotNil(displayString);
}

- (void)testDateFromRSSDateString {
    NSString *rssDateString = @"Wed, 05 Sep 2018 10:00:00 PDT";
    NSDate *parsedRSSDate = [NSDate dmm_dateFromRSSDateString:rssDateString];

    XCTAssertNotNil(parsedRSSDate);

    NSString *invalidParsedRSSDate = @"Wednesday, 5 September 2018 2:00:00 PM PDT";
    XCTAssertNil([NSDate dmm_dateFromRSSDateString:invalidParsedRSSDate]);
}

@end
