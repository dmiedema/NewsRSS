//
//  NewsItem+CoreDataProperties.m
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//
//

#import "NewsItem+CoreDataProperties.h"

@implementation NewsItem (CoreDataProperties)

+ (NSFetchRequest<NewsItem *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"NewsItem"];
}

@dynamic guid;
@dynamic title;
@dynamic read;
@dynamic link;
@dynamic date;
@dynamic itemDescription;
@dynamic encodedContent;
@dynamic imageURL;
@dynamic image;

@end
