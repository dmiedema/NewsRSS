//
//  NewsItem+CoreDataClass.h
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsItem : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "NewsItem+CoreDataProperties.h"
