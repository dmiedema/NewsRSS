//
//  NewsItem+CoreDataProperties.h
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//
//

#import "NewsItem+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface NewsItem (CoreDataProperties)

+ (NSFetchRequest<NewsItem *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *guid;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) BOOL read;
@property (nullable, nonatomic, copy) NSString *link;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *itemDescription;
@property (nullable, nonatomic, copy) NSString *encodedContent;
@property (nullable, nonatomic, copy) NSString *imageURL;
@property (nullable, nonatomic, retain) NSData *image;

@end

NS_ASSUME_NONNULL_END
