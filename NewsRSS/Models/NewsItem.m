//
//  NewsItem.m
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NewsItem.h"
#import "NSPersistentContainer+Helpers.h"
#import "NSDate+Helpers.h"
#import "UIImage+Helpers.h"

@implementation NewsItem (Helpers)
- (void)updateWithContent:(NSString *)content forElementName:(NSString *)elementName {
    // Trim off leading/trailing newlines & whitespace
    content = [content stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // handle edge cases for `elementName` up front.
    // otherwise we can do dirty things like `setValue:forKey`
    // because we align with _some_ of our XML keys
    if ([elementName isEqualToString:@"description"]) {
        self.itemDescription = content;
    } else if ([elementName isEqualToString:@"pubDate"]) {
        self.date = [NSDate dmm_dateFromRSSDateString:content];
    } else if ([elementName isEqualToString:@"content:encoded"]) {
        self.encodedContent = content;

        [self parseItemsFromEncodedContent:content];
    } else if ([self respondsToSelector:NSSelectorFromString(elementName)]) {
        [self setValue:content forKey:elementName];
    } else {
        NSLog(@"Found Key ('%@') that is not available on our NewsItem object", elementName);
    }
}

+ (instancetype)createOrUpdateWithNewsItem:(NewsItem *)newsItem {
    return [self createOrUpdateWithNewsItem:newsItem inContext:nil];
}

+ (instancetype)createOrUpdateWithNewsItem:(NewsItem *)newsItem inContext:(NSManagedObjectContext *)passedContext {
    NSManagedObjectContext *context = passedContext ?: NSPersistentContainer.defaultContainer.viewContext;
    __auto_type item = [NewsItem byGUID:newsItem.guid inContext:context];
    if (!item) {
        item = [[NewsItem alloc] initWithContext:context];
    }

    item.date = newsItem.date ?: item.date;
    item.encodedContent = newsItem.encodedContent ?: newsItem.encodedContent;
    item.guid = newsItem.guid ?: item.guid;
    item.read = newsItem.read || item.read;
    item.title = newsItem.title ?: item.title;
    item.link = newsItem.link ?: item.link;
    item.itemDescription = newsItem.itemDescription ?: item.itemDescription;
    item.imageURL = newsItem.imageURL ?: item.imageURL;
    item.image = newsItem.image ?: item.image;

    return item;
}

+ (instancetype)byGUID:(NSString *)guid inContext:(NSManagedObjectContext *)passedContext {
    NSManagedObjectContext *context = passedContext ?: NSPersistentContainer.defaultContainer.viewContext;
    __auto_type fetchRequest = NewsItem.fetchRequest;
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"guid = %@", guid];
    NSError *error;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    if (results.count > 1 || error) {
        NSLog(@"ERROR: %@", error);
    }
    return results.firstObject;
}

+ (NSArray<NewsItem *> *)allItems {
    return [NewsItem allItemsInContext:nil];
}

+ (NSArray<NewsItem *> *)allItemsInContext:(NSManagedObjectContext *)context {
    NSManagedObjectContext *fetchContext = context ?: NSPersistentContainer.defaultContainer.viewContext;
    __auto_type fetchRequest = NewsItem.fetchRequest;
    NSError *error;
    __auto_type results = [fetchContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"ERROR: %@", error);
    }
    return results;
}

+ (NSArray<NewsItem *> *)allItemsSortedByDate {
    return [NewsItem allItemsSortedByDateInContext:nil];
}

+ (NSArray<NewsItem *> *)allItemsSortedByDateInContext:(NSManagedObjectContext *)context {
    return [[NewsItem allItemsInContext:context] sortedArrayUsingComparator:^NSComparisonResult(NewsItem *_Nonnull obj1, NewsItem *_Nonnull obj2) {
        return [obj2.date compare:obj1.date];
    }];
}

#pragma mark - Internal
- (void)parseItemsFromEncodedContent:(nonnull NSString *)content {
    // Parse our are image url (if we have one)
    if (![content containsString:@"img src="]) { return; }
    NSString *imgString = [content componentsSeparatedByString:@"img src="].lastObject;
    if (!imgString) { return; }
    __auto_type scanner = [NSScanner scannerWithString:imgString];
    NSString *imageURL;
    [scanner scanUpToString:@" " intoString:&imageURL];
    NSLog(@"imageURL: %@", imageURL);
    // Remove the " from around our URL
    imageURL = [imageURL stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSURL *url = [NSURL URLWithString:imageURL];
    if (url) {
        self.imageURL = imageURL;
        if (!self.image) {
            __auto_type request = [NSURLRequest requestWithURL:url];
            __auto_type task = [NSURLSession.sharedSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    __auto_type image = [UIImage imageWithData:data];
                    if (image) {
                        // scale our image down to 100x100 so
                        // it doesn't take over/shift stuff around
                        // a ton by being too huge.
                        __auto_type scaledImage = [image dmm_scaleImageToSize:CGSizeMake(100, 100)];
                        self.image = UIImagePNGRepresentation(scaledImage);
                    }
                }
            }];
            [task resume];
        }
    }
}

@end
