//
//  NewsItem.h
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NewsItem+CoreDataClass.h"
#import "NewsItem+CoreDataProperties.h"

@interface NewsItem (Helpers)
/// Update our self with some content for a given XML element nane
/// @param content    NSString content to update ourselves with
/// @param elementName XML Element Name
- (void)updateWithContent:(nonnull NSString *)content forElementName:(nonnull NSString *)elementName;

/// Create or Update a given NewsItem in a the viewContext
/// @param newsItem NewsItem to use to create/update from
/// @return Newly Created or Updated NewsItem in the given Context
+ (nonnull instancetype)createOrUpdateWithNewsItem:(nonnull NewsItem *)newsItem;

/// Create or Update a given NewsItem in a given NSManagedObjectContext
/// @param newsItem NewsItem to use to create/update from
/// @param context  Optional NSManagedObjectContext to create or update in. If no context is passed, the `viewContext` is used
/// @return Newly Created or Updated NewsItem in the given Context
+ (nonnull instancetype)createOrUpdateWithNewsItem:(nonnull NewsItem *)newsItem inContext:(nullable NSManagedObjectContext *)context;

/// Get a given NewsItem by a specified GUID in a given context
/// @param guid     `NSString` GUID of the news item
/// @param context  Optional NSManagedObjectContext to create or update in. If no context is passed, the `viewContext` is used
/// @return `nil` if no `NewsItem` is found, otherwise the news item will be returned
+ (nullable instancetype)byGUID:(nonnull NSString *)guid inContext:(nullable NSManagedObjectContext *)context;

/// Get all News Items
@property (class, nonatomic, nonnull, readonly, copy) NSArray <NewsItem *> *allItems;

/// Get all the News Items sorted by their publication date
@property (class, nonatomic, nonnull, readonly, copy) NSArray <NewsItem *> *allItemsSortedByDate;

/// Get all News Items from a specific Context
+ (NSArray <NewsItem *> *)allItemsInContext:(nullable NSManagedObjectContext *)context;

/// Get all the News Items sorted by their publication date
/// from a specific context
+ (NSArray <NewsItem *> *)allItemsSortedByDateInContext:(nullable NSManagedObjectContext *)context;

@end
