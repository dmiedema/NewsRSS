//
//  NSDate+Helpers.h
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (Helpers)
/// Get a Date object from an RSS feed date string
+ (nonnull instancetype)dmm_dateFromRSSDateString:(nonnull NSString *)string;

/// Get our date as a nice, pretty displayable string
@property (nonatomic, nonnull, readonly, copy) NSString *dmm_displayString;
@end

NS_ASSUME_NONNULL_END
