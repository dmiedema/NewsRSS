//
//  NSPersistentContainer+Helpers.h
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSPersistentContainer (Helpers)
/// Access the peristent container stored in our app delegate
@property (class, nonatomic, nonnull, readonly, strong) NSPersistentContainer *defaultContainer;

/// Delete all our current stores
- (void)deleteStores;

@end

NS_ASSUME_NONNULL_END
