//
//  UIImage+Helpers.h
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helpers)
/// Scale a given `UIImage` to a new size while
/// maintaining the aspect ratio.
/// @param newSize the CGSize to scale our image to
/// @return self (`UIImage`) scaled to `newSize`
- (nonnull UIImage *)dmm_scaleImageToSize:(CGSize)newSize;
@end
