//
//  NSPersistentContainer+Helpers.m
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NSPersistentContainer+Helpers.h"

@implementation NSPersistentContainer (Helpers)
+ (NSPersistentContainer *)defaultContainer {
    static NSPersistentContainer *container = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        container = [[NSPersistentContainer alloc] initWithName:@"NewsRSS"];

        for (NSPersistentStoreDescription *storeDescription in container.persistentStoreDescriptions) {
            storeDescription.shouldMigrateStoreAutomatically = YES;
        }

        [container loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription * _Nonnull storeDescription, NSError * _Nullable error) {
            if (error) {
                [container deleteStores];
                NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                abort();
            }
        }];
    });
    return container;
}

- (void)deleteStores {
    for (NSPersistentStoreDescription *storeDescription in self.persistentStoreDescriptions) {

        [self.persistentStoreCoordinator destroyPersistentStoreAtURL:storeDescription.URL withType:storeDescription.type options:nil error:nil];
    }
}
@end
