//
//  NSDate+Helpers.m
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NSDate+Helpers.h"

@implementation NSDate (Helpers)
#pragma mark - Formatters
+ (NSDateFormatter *)dmm_RSSDateFormatter {
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        // Gotta parse        `Fri, 31 Aug 2018 13:30:00 PDT`
        formatter.dateFormat = @"E, dd MMM yyyy HH:mm:ss zzz";
    });
    return formatter;
}

+ (NSDateFormatter *)dmm_shortFormatDateFormatter {
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateStyle = NSDateFormatterShortStyle;
        formatter.timeStyle = NSDateFormatterShortStyle;
    });
    return formatter;
}


#pragma mark - Implementation
+ (instancetype)dmm_dateFromRSSDateString:(NSString *)string {
    return [[NSDate dmm_RSSDateFormatter] dateFromString:string];
}

- (NSString *)dmm_displayString {
    return [[NSDate dmm_shortFormatDateFormatter] stringFromDate:self];
}
@end
