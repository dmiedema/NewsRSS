//
//  main.m
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
