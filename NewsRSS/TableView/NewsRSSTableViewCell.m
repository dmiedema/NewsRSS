//
//  NewsRSSTableViewCell.m
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NewsRSSTableViewCell.h"
#import "NewsItem.h"
#import "NSDate+Helpers.h"

NSString * const NewsRSSTableViewCellIdentifier = @"NewsRSSTableViewCellIdentifier";

@interface NewsRSSTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation NewsRSSTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.itemImageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)configureWithItem:(NewsItem *)item {
    if (item.image) {
        self.itemImageView.image = [UIImage imageWithData:item.image];
    } else {
        self.itemImageView.image = nil;
    }
    
    self.titleLabel.text = item.title;
    self.descriptionLabel.text = item.itemDescription;
    self.dateLabel.text = item.date.dmm_displayString;

    self.contentView.backgroundColor = item.read
        ? UIColor.whiteColor
        : [UIColor.blueColor colorWithAlphaComponent:0.10];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.imageView.image = nil;
}

@end
