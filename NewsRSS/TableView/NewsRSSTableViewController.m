//
//  NewsRSSTableViewController.m
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NewsRSSTableViewController.h"
#import "NewsItem.h"
#import "NewsTableViewDatasource.h"
#import "NewsRSSTableViewCell.h"
#import <SafariServices/SafariServices.h>

@interface NewsRSSTableViewController () <UISearchResultsUpdating, UISearchBarDelegate, NewsTableViewDatasourceDelegate>
@property (nonatomic, nonnull, strong) UISearchController *searchController;
@property (nonatomic, nonnull, strong) NSArray <NewsItem *> *filteredItems;
@property (nonatomic, nonnull, strong) NewsTableViewDatasource *datasource;
@end

@implementation NewsRSSTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Apple Developer News", @"News TableView Controller Title");
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NewsRSSTableViewCell" bundle:nil] forCellReuseIdentifier:NewsRSSTableViewCellIdentifier];

    self.tableView.tableHeaderView = [UIView new];
    self.tableView.tableFooterView = [UIView new];

    self.navigationItem.searchController = self.searchController;

    if (!self.refreshControl) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl = refreshControl;
    }
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];


    self.filteredItems = [self.datasource newsItemsFilteredWithText:self.searchController.searchBar.text];

    [self refresh];
}

#pragma mark - Properties

- (UISearchController *)searchController {
    if (!_searchController) {
        _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        _searchController.searchResultsUpdater = self;
        _searchController.dimsBackgroundDuringPresentation = NO;

        _searchController.searchBar.delegate = self;
        self.definesPresentationContext = YES;
    }
    return _searchController;
}

- (NewsTableViewDatasource *)datasource {
    if (!_datasource) {
        _datasource = [NewsTableViewDatasource new];
        _datasource.delegate = self;
    }
    return _datasource;
}

#pragma mark - Implementation
- (void)refresh {
    [self.refreshControl beginRefreshing];
    [self.datasource refreshNewsItems];
}

#pragma mark - Table View Cells

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredItems.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NewsRSSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsRSSTableViewCellIdentifier forIndexPath:indexPath];
    NewsItem *newsItem = self.filteredItems[indexPath.row];

    [cell configureWithItem:newsItem];

    return cell;
}

#pragma mark - Table View Editing

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

#pragma mark - Table View Selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert((NSUInteger)indexPath.row <= self.filteredItems.count,
             @"Attempt to fetch item beyond `self.filteredItems` count. Fetched %@ when we only have %@ items",
             @(indexPath.row), @(self.filteredItems.count));

    NewsItem *newsItem = self.filteredItems[indexPath.row];

//    BOOL previousReadState = newsItem.read;
    newsItem.read = YES;

    // save that this item is read
//    NSError *error;
//    if (![newsItem.managedObjectContext save:&error]) {
//        NSLog(@"ERROR: Failed to save read state for item. %@", error);
//        newsItem.read = previousReadState;
//    }

    NSURL *url = [NSURL URLWithString:newsItem.link];
    if (!url) {
        NSString *messageFormatString = NSLocalizedString(@"Feed item URL was not a valid URL as far as we can tell! '%@'", nil);
        NSString *message = [NSString stringWithFormat:messageFormatString, newsItem.link];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:message preferredStyle:(UIAlertControllerStyleAlert)];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    __auto_type safariController = [[SFSafariViewController alloc] initWithURL:url];
    safariController.dismissButtonStyle = SFSafariViewControllerDismissButtonStyleClose;

    [self presentViewController:safariController animated:YES completion:nil];

    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - UISearchResultsUpdating
- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {
    // Just reload, filteredItems will take our serachController
    // text into account and update the results
    [self.tableView reloadData];
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.filteredItems = [self.datasource newsItemsFilteredWithText:nil];
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.filteredItems = [self.datasource newsItemsFilteredWithText:searchText];
}

#pragma mark - NewsTableViewDatasourceDelegate
- (void)newsTableDatasource:(NewsTableViewDatasource *)dataSource didFinishRefreshingNewsItemsWithError:(NSError *)error {
    [self.refreshControl endRefreshing];
    if (error) {
        UIAlertController *alert =[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error Refreshing Items", @"Error refreshing items alert title") message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK button string") style:UIAlertActionStyleDefault handler:nil]];

        [self presentViewController:alert animated:YES completion:nil];
    } else {
        self.filteredItems = [self.datasource newsItemsFilteredWithText:self.searchController.searchBar.text];
    }
    [self.tableView reloadData];
}

@end
