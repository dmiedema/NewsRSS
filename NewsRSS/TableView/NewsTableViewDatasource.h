//
//  NewsTableViewDatasource.h
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NewsItem;
@class NSManagedObjectContext;
// Originally I was going to use a block to communicate completion of the loading
// but rather than dealing with copying the block internally and
// managing/doing the  weakSelf/strongSelf dance just using a delegate
// seemed easier.
@protocol NewsTableViewDatasourceDelegate;

@interface NewsTableViewDatasource : NSObject
/// Delegate to inform of irem refreshing.
@property (nonatomic, nullable, weak) id<NewsTableViewDatasourceDelegate> delegate;

/// Create a datasource with a specific context.
/// if `nil` is passed it'll use the defaultContainer
- (nonnull instancetype)initWithContext:(nullable NSManagedObjectContext *)context;

/// Get our news items filtered by an optionally given string.
/// @discussion The properties on `NewsItem`s that will
/// be searched through to contain the given `string` are
/// @li `title`
/// @li `itemDescription`
/// @li `link`
/// @param text Our text to filter our news items by
/// @return All our `NewsItem`s sorted by date fitlered by our string
- (nonnull NSArray <NewsItem *> *)newsItemsFilteredWithText:(nullable NSString *)text;

/// Fresh our news items via the RSS feed.
- (void)refreshNewsItems;
@end

@protocol NewsTableViewDatasourceDelegate <NSObject>
/// Method called by our datasource after our news items
/// have been refreshed.
/// @param dataSource `NewsTableViewDatasource` that generated the message
/// @param error      `NSError` that occured loading/processing `NewsItem`s
- (void)newsTableDatasource:(nonnull NewsTableViewDatasource *)dataSource didFinishRefreshingNewsItemsWithError:(nullable NSError *)error;
@end
