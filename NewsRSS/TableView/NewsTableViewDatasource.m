//
//  NewsTableViewDatasource.m
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NewsTableViewDatasource.h"
#import "NewsItem.h"
#import "NewsXMLParser.h"
#import "NSPersistentContainer+Helpers.h"

@interface NewsTableViewDatasource () <NewsXMLParserDelegate>
@property (nonatomic, nullable, strong) NewsXMLParser *parser;
@property (nonatomic, nonnull, strong) NSManagedObjectContext *context;
@end

@implementation NewsTableViewDatasource
#pragma mark - Init
- (instancetype)init {
    return [self initWithContext:nil];
}

- (instancetype)initWithContext:(NSManagedObjectContext *)context {
    if (self = [super init]) {
        _context = context ?: NSPersistentContainer.defaultContainer.viewContext;
    }
    return self;
}

#pragma mark - Properties
- (NewsXMLParser *)parser {
    if (!_parser) {
        _parser = [NewsXMLParser newsXMLParserWithDelegate:self];
    }
    return _parser;
}

#pragma mark - Implementation
- (NSArray<NewsItem *> *)newsItemsFilteredWithText:(NSString *)text {
    NSArray *sortedItems = [NewsItem allItemsSortedByDateInContext:self.context];
    if (text && text.length > 0) {
        sortedItems = [sortedItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NewsItem *_Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [evaluatedObject.title localizedCaseInsensitiveContainsString:text]
            || [evaluatedObject.itemDescription localizedCaseInsensitiveContainsString:text]
            || [evaluatedObject.link localizedCaseInsensitiveContainsString:text];
        }]];
    }
    return sortedItems;
}

- (void)refreshNewsItems {
    // Super lazy way to do this, just use NSURLSession directly rather than creating a wrapper around it
    // to create requests for me, etc. Since we only have the single request its just not really worth
    // creating a bunch of classes/tests.
    NSURL *url = [NSURL URLWithString:@"https://developer.apple.com/news/rss/news.rss"];
    // NSURL *url = [NSURL URLWithString:@"https://www.macstories.net/feed/"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    __auto_type task = [NSURLSession.sharedSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error || !data) {
            error = error ?: [NSError errorWithDomain:NewsXMLParserErrorDomain code:NewsXMLParserErrorCodeNoData userInfo:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate newsTableDatasource:self didFinishRefreshingNewsItemsWithError:error];
            });
        } else {
            [self.parser parse:data];
        }
    }];
    [task resume];
}

#pragma mark - NewsXMLParserDelegate
- (void)newsXMLParserDidEndParsingWithResults:(NSArray<NewsItem *> *)results error:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        for (NewsItem *item in results) {
            [NewsItem createOrUpdateWithNewsItem:item inContext:self.context];
        }
        [self.delegate newsTableDatasource:self didFinishRefreshingNewsItemsWithError:error];
    });
}
@end
