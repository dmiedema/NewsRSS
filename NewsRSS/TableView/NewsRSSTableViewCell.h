//
//  NewsRSSTableViewCell.h
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NewsItem;

extern NSString * const NewsRSSTableViewCellIdentifier;

@interface NewsRSSTableViewCell : UITableViewCell
/// Configure our NewsRSSTableViewCell with a given `NewsItem`
- (void)configureWithItem:(nonnull NewsItem *)item;
@end

