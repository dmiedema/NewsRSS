//
//  NewsRSSTableViewController.h
//  NewsRSS
//
//  Created by miedema on 9/7/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsRSSTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
