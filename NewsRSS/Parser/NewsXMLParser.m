//
//  NewsXMLParser.m
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import "NewsXMLParser.h"
#import "NSPersistentContainer+Helpers.h"
#import "NewsItem.h"

NSString * const NewsXMLParserErrorDomain = @"NewsXMLParserErrorDomain";

@interface NewsXMLParser () <NSXMLParserDelegate>
@property (nonatomic, nonnull, strong) NSXMLParser *parser;
@property (nonatomic, nullable, strong) NSError *parseError;
@property (nonatomic, nonnull, strong) NSMutableArray <NewsItem *> *results;

@property (nonatomic, nullable, strong) NewsItem *currentItem;
@property (nonatomic, nullable, copy) NSString *elementName;
@property (nonatomic, nonnull, strong) NSString *elementContents;

@property (nonatomic, nonnull, strong) NSManagedObjectContext *parserContext;
@end

@implementation NewsXMLParser

#pragma mark - Init
+ (instancetype)newsXMLParserWithDelegate:(id<NewsXMLParserDelegate>)deletage {
    NewsXMLParser *parser = [NewsXMLParser new];
    parser.delegate = deletage;

    return parser;
}

- (instancetype)init {
    if (self = [super init]) {
        _results = [NSMutableArray array];
        _elementContents = [NSMutableString string];
    }
    return self;
}

#pragma mark - Implementation
- (void)parse:(NSData *)data {
    self.data = data;
    [self parseData];
}

- (void)parseData {
    if (!self.data) {
        NSError *error = [NSError errorWithDomain:NewsXMLParserErrorDomain code:NewsXMLParserErrorCodeNoData userInfo:@{NSLocalizedDescriptionKey: @"No data to Parse"}];
        [self.delegate newsXMLParserDidEndParsingWithResults:@[] error:error];
        return;
    }
    _parser = [[NSXMLParser alloc] initWithData:self.data];
    _parser.delegate = self;
    if (![_parser parse]) {
        NSError *error = [NSError errorWithDomain:NewsXMLParserErrorDomain code:NewsXMLParserErrorCodeParseFailure userInfo:@{NSLocalizedDescriptionKey: @"Failed to parse the data"}];
#if DEBUG
        @throw error;
#else
        [self.delegate newsXMLParserDidEndParsingWithResults:@[] error:error];
#endif
    }
}

- (NSManagedObjectContext *)parserContext {
    if (!_parserContext) {
        _parserContext = NSPersistentContainer.defaultContainer.newBackgroundContext;
    }
    return _parserContext;
}

#pragma mark - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    // When we start a new `item` entry we need to make a new
    // news item object to store our content in
    if ([elementName isEqualToString:@"item"]) {
        if (self.currentItem) {
            // dis bad
            NSString *error = @"We have a currentItem in our parser but we started a new `item`";
#if DEBUG
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:error userInfo:nil];
#else
            NSLog(@"ERROR: %@", error);
#endif
        } else {
            self.currentItem = [[NewsItem alloc] initWithContext:self.parserContext];
        }
    }
    self.elementName = elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    self.elementContents = [self.elementContents stringByAppendingString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
        // save our item
        [self.results addObject:self.currentItem];
        // and make sure its `nil` before we start our next 'item"
        self.currentItem = nil;
    }

    if (!self.currentItem) {
        // no need to process anything so we'll just bail and move on
        self.elementName = nil;
        self.elementContents = @""; // make sure we clear out our elementContents string
        return;
    }

    if (self.elementContents && self.elementName) {
        [self.currentItem updateWithContent:self.elementContents forElementName:self.elementName];
    }
    self.elementName = nil;
    self.elementContents = @""; // make sure we clear out our elementContents string
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    self.parseError = parseError;
    [self.delegate newsXMLParserDidEndParsingWithResults:@[] error:parseError];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [self.delegate newsXMLParserDidEndParsingWithResults:self.results error:self.parseError];
}
@end
