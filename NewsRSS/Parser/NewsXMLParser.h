//
//  NewsXMLParser.h
//  NewsRSS
//
//  Created by miedema on 9/6/18.
//  Copyright © 2018 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NewsItem;
@protocol NewsXMLParserDelegate;

/// Enum to represent our error codes
typedef NS_ENUM(NSUInteger, NewsXMLParserErrorCode) {
    /// Code returned when no data is specified to be parsed
    NewsXMLParserErrorCodeNoData,
    /// Code for when there was a general failure with parsing our RSS feed XML
    NewsXMLParserErrorCodeParseFailure,
};
/// Parser error domain
extern NSString * const NewsXMLParserErrorDomain;

@interface NewsXMLParser : NSObject
/// Data to parse
@property (nonatomic, nonnull, strong) NSData *data;
/// Delegate to inform of parsing events
@property (nonatomic, nullable, weak) id<NewsXMLParserDelegate> delegate;

/// Create a NewsXMLParser with a given delegate
+ (nonnull instancetype)newsXMLParserWithDelegate:(nullable id<NewsXMLParserDelegate>)deletage;

/// Tell our parser to parse our current `data`
- (void)parseData;

/// Tell our parser to parse some given data
- (void)parse:(nonnull NSData *)data;

@end

@protocol NewsXMLParserDelegate <NSObject>

/// Notify our delegate we finished parsing our data
/// @param results Array of NewsItem results if parsing was successful. Will be empty if we encoutered an error
/// @param error `nil` if parsing was successful, populated with applicable error if something went wrong
- (void)newsXMLParserDidEndParsingWithResults:(nonnull NSArray <NewsItem *> *)results error:(nullable NSError *)error;

@end
