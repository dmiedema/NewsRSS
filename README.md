# NewsRSS

Basic RSS Reader.

### Notes

Storyboard: Just easier to use for initial view controller configuration. Since there aren't many view controllers all one of them just end up living in the storyboard.

nib for tableview cell: easier to visualize the layout. Even though the cell is simple, maintainability isn't really the biggest concern and it just makes life a lot easier when dealing with stackviews. Visualizing how they'll lay out as well as the faster feedback loop on constraint issues makes them nice for super simple projects

I tried my best to handle images in the `content:encoded` key however it I didn't see any in the apple developer feed.
I did try the macstories rss feed to handle `img` tags and extract the `src` from those though.

Local storage can cleared in Settings -> NewsRSS -> Clear Local RSS Feed Data. Then terminating & relaunching the application. This deletes the local coredata store so all the data can be recreated.

